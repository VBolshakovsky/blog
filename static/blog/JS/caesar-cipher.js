window.addEventListener('DOMContentLoaded', function() {
  
  var textIn = document.getElementById('text-in');
  var stepElement = document.getElementById('step');
  var step = Number(stepElement.value);
  var result = document.getElementById('text-out');
  var btnEncrypt = document.getElementById('btn-encrypt');
  var btnDecrypt = document.getElementById('btn-decrypt');
  var btnReset = document.getElementById('btn-reset');
  var alphabet = document.getElementById('ABC');
  var alphabetEncrypt = document.getElementById('ABC-encrypt');
  var OtherSymbols = [' ',',','.',':',';','!','?','-','_','=','+','(',')','[',']','@','`',"'",'"','<','>','|','/','%','$','^','&','*','~'];
  var Numbers = ['0','1','2','3','4','5','6','7','8','9'];
  var alphabetRusUp = ['А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И', 'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т', 'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь', 'Э', 'Ю', 'Я'];
  var alphabetRusLower = ['а', 'б', 'в', 'г', 'д', 'е', 'ё', 'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п', 'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ', 'ъ', 'ы', 'ь', 'э', 'ю', 'я'];
  var alphabetEngUp = ['A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z'];
  var alphabetEngLower = ['a','b','c','d','e','f','g','h','i','j','k','l','m','m','o','p','q','r','s','t','u','v','w','x','y','z'];
  var alphabetRusUpEncrypt = Array(33);
  var alphabetRusLowerEncrypt = Array(33);
  var alphabetEngUpEncrypt = Array(26); 
  var alphabetEngLowerEncrypt = Array(26);
  var NumbersEncrypt = Array(10);
  var textTemp;
  var pos;

  alphabet.value = alphabetRusUp;

  initEncrypt();

  stepElement.addEventListener('change', function() {
    step = Number(this.value);
    initEncrypt();
  });
  
  function initEncrypt() {
    alphabetRusUpEncrypt = caesarCipher(step, alphabetRusUp);
    alphabetRusLowerEncrypt = caesarCipher(step, alphabetRusLower);
    NumbersEncrypt = caesarCipher(step, Numbers);
    alphabetEngUpEncrypt = caesarCipher(step, alphabetEngUp);
    alphabetEngLowerEncrypt = caesarCipher(step, alphabetEngLower);
  }  


  function caesarCipher(stap, arr) {
    var CopyAlf = arr.slice();
    var i = 0;
    
    while ((i + stap) < (CopyAlf.length)) {
      var buff = CopyAlf[i];
      CopyAlf[i] = CopyAlf[i + stap];
      CopyAlf[i + stap] = buff;
      i++;     
    }
    return CopyAlf;
  }
  
  function contains(symb, arr) {
    var letter = symb;
    pos = 0;
    for (var i = 0; i < arr.length; i++) {
      if (letter === arr[i]) {
        pos = i;
        return true;
        break;
      }
    }
  }
  
  function encrypt(text) {
    var result = '';
    for (var i = 0; i <= text.length; i++) {
      var symbol = text[i];
      if (contains(symbol, OtherSymbols)) {
        result += symbol;
      }
      if (contains(symbol, Numbers)) {
        symbol = NumbersEncrypt[pos];
        result += symbol;
      }
      if (contains(symbol, alphabetRusUp)) {
          symbol = alphabetRusUpEncrypt[pos];
          result += symbol;
      }
      if ((contains(symbol, alphabetRusLower))) {
          symbol = alphabetRusLowerEncrypt[pos];
          result += symbol;
      }
      if (contains(symbol, alphabetEngUp)) {
          symbol = alphabetEngUpEncrypt[pos];
          result += symbol;
      }
      if ((contains(symbol, alphabetEngLower))) {
          symbol = alphabetEngLowerEncrypt[pos];
          result += symbol;
      }
    }
    return result;
  }
  
  function decrypt(text) {
    var result = '';
    for (var i = 0; i <= text.length; i++) {
      var symbol = text[i];
      if (contains(symbol, OtherSymbols)) {
        result += symbol;
      }
      if (contains(symbol, NumbersEncrypt)) {
        symbol = Numbers[pos];
        result += symbol;
      }
      if (contains(symbol, alphabetRusUpEncrypt)) {
          symbol = alphabetRusUp[pos];
          result += symbol;
      }
      if ((contains(symbol, alphabetRusLowerEncrypt))) {
          symbol = alphabetRusLower[pos];
          result += symbol;
      }
      if (contains(symbol, alphabetEngUpEncrypt)) {
          symbol = alphabetEngUp[pos];
          result += symbol;
      }
      if ((contains(symbol, alphabetEngLowerEncrypt))) {
          symbol = alphabetEngLower[pos];
          result += symbol;
      }
    }
    return result;
  }
  
  window.onload = function() {
      textTemp = textIn.value;
      result.value = encrypt(textTemp);
      alphabetEncrypt.value = alphabetRusUpEncrypt;
  };

  btnEncrypt.addEventListener('click', function() {
    textTemp = textIn.value;
    result.value = encrypt(textTemp);
    alphabetEncrypt.value = alphabetRusUpEncrypt;
  });
  btnDecrypt.addEventListener('click', function() {
    textTemp = textIn.value;
    result.value = decrypt(textTemp);
  });
  btnReset.addEventListener('click', function() {
    textIn.value = '';
    result.value = '';
  });
});