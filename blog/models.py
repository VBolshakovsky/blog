from distutils.command.upload import upload
from django.db import models
from ckeditor.fields import RichTextField
from django.urls import reverse

# Create your models here.

BLOG_STATUS_CHOICE = (
    ('Formed', 'Formed'),
    ('Published', 'Published'),
    ('Deleted', 'Deleted')
)

class Tag(models.Model):
    name = models.CharField(max_length=20, unique=True, verbose_name='Название')
    slug = models.CharField(max_length=20, unique=True)

    def __str__(self):
        return self.name
    
    def get_absolute_url(self):
        return reverse('tags', kwargs={'tags_slug': self.slug})
    
    class Meta:
        verbose_name='Тэги'
        verbose_name_plural = 'Тэги'

class Category(models.Model):
    name = models.CharField(max_length=20, unique=True, verbose_name='Название')
    slug = models.CharField(max_length=20, unique=True)
    
    def __str__(self):
        return self.name
    
    def get_absolute_url(self):
        return reverse('category', kwargs={'category_slug': self.slug})
    
    class Meta:
        verbose_name='Категории'
        verbose_name_plural = 'Категории'

class Article(models.Model):
    title = models.CharField(max_length=100, unique=True, verbose_name='Заголовок')
    phrase = models.CharField(max_length=100, unique=True, default='', verbose_name='Фраза под заголовком')
    slug = models.SlugField(max_length=100, unique=True, db_index=True)
    created = models.DateTimeField(auto_now_add=True, verbose_name='Создано')
    updated = models.DateField(auto_now=True, verbose_name='Обновлено')
    is_published = models.BooleanField(default=True, verbose_name='Опубликовано')
    status = models.CharField(max_length=10, choices=BLOG_STATUS_CHOICE, default='Formed', verbose_name='Статус')
    tags = models.ManyToManyField(Tag, related_name='related_posts', verbose_name='Тэги')
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=True, blank=True, verbose_name='Категория')
    meta_description = models.TextField(max_length=160, null=True, blank=True, verbose_name='meta описание')
    meta_keywords = models.TextField(max_length=160, null=True, blank=True, verbose_name='meta ключевые слова')
    meta_author = models.TextField(max_length=100, null=True, blank=True, verbose_name='meta автор')
    preview_img = models.ImageField(upload_to='pictures/', null=True, blank=True,)
    preview_text = models.TextField(max_length=2000, null=True, blank=True, verbose_name='Превью текст')
    content = RichTextField(verbose_name='Содержание')
    
    def __str__(self):
        return self.title
    
    def get_absolute_url(self):
        return reverse('article', kwargs={'article_slug': self.slug})
    
    def get_month_inwords(self):
        month_list = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня',
        'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря']
        return (str(self.created.day) + ' ' + month_list[int(self.created.month)-1])
        
        
    class Meta:
        verbose_name='Статья'
        verbose_name_plural = 'Статья'
