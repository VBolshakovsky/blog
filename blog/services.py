def get_yaer_list(article):
    #Функция предоставляет список лет, для сортировки статей в archive.
    #Вход: queryset статей.
    year = 0
    year_list = []
    for a in article:
        if a.created.year != year:
            year_list.append({'year':int(a.created.year)})
            year = a.created.year
    return year_list