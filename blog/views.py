from unicodedata import name
from django.http import HttpResponse, HttpResponseNotFound
from django.shortcuts import render, get_object_or_404
from blog.services import get_yaer_list
from django.core.paginator import Paginator
from blog.models import Article,Tag,Category
from django.db.models import Q
# Create your views here.
article_page_count = 3

menu_sort = [{'title': "по тегам", 'url': 'tags_all', 'active': False},
             {'title': "по категории", 'url': 'category_all', 'active': False}
            ]

def pageNotFound(request,exception):
    return render(request, 'blog/404.html')

def index(request):
    
    searsh_query = request.GET.get('search', '')
    
    if searsh_query:
        article = Article.objects.filter(Q(title__icontains=searsh_query) | Q(content__icontains=searsh_query)).order_by('-created')
    else:
        article = Article.objects.filter(status='Published').order_by('-created')

    paginator = Paginator(article, article_page_count)
    
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    
    if page_obj.has_next():
        next_url = f'?page={page_obj.next_page_number()}'
    else:
        next_url = ''
        
    if page_obj.has_previous():
        prev_url = f'?page={page_obj.previous_page_number()}'
    else:
        prev_url=''
        
    context = { 'page_obj': page_obj,
               'next_page_url': next_url,
               'prev_page_url': prev_url
    }
    return render(request, 'blog/index.html', context = context)

def show_article(request, article_slug):
    article = get_object_or_404(Article, slug=article_slug)
    context = { 'article': article
    }
    return render(request, 'blog/article.html', context=context)

def archive(request):
    article = Article.objects.all().order_by('-created')
    context = { 'article': article,
                'year_list': get_yaer_list(article),
                'object_sort': 'Весь архив статей',
                'list_sort': 'tag_list',
                'menu_sort': menu_sort,
                'name_sort': 'Тэги:',
                }
    menu_sort[0]['active'] = False
    menu_sort[1]['active'] = False
    
    return render(request, 'blog/archive.html', context=context)

def show_tags(request, tags_slug=None):
    tag_list = Tag.objects.all()
    
    if tags_slug != None:
        tag = get_object_or_404(Tag, slug=tags_slug)
        article = Article.objects.all().filter(tags=tag).order_by('-created')
        
        context = { 'article': article,
                    'year_list': get_yaer_list(article),
                    'object_sort': tag.name,
                    'list_sort': tag_list,
                    'menu_sort': menu_sort,
                    'name_sort': 'Тэги:',
                    }
        
    else:
        article = Article.objects.all().order_by('-created')
        context = { 'article': article,
                    'year_list': get_yaer_list(article),
                    'object_sort': 'Все тэги',
                    'list_sort': tag_list,
                    'menu_sort': menu_sort,
                    'name_sort': 'Тэги:',
                    }        
    
    menu_sort[0]['active'] = True
    menu_sort[1]['active'] = False
    

    
    return render(request, 'blog/archive.html', context=context)

def show_category(request, category_slug=None):
    category_list = Category.objects.all()
    
    if category_slug != None:
        category = get_object_or_404(Category, slug=category_slug)
        article = Article.objects.all().filter(category=category).order_by('-created')
        
        context = { 'article': article,
                    'year_list': get_yaer_list(article),
                    'object_sort': category.name,
                    'list_sort': category_list,
                    'menu_sort': menu_sort,
                    'name_sort': 'Тэги:',
                    }
        
    else:
        article = Article.objects.all().order_by('-created')
        context = { 'article': article,
                    'year_list': get_yaer_list(article),
                    'object_sort': 'Все категории',
                    'list_sort': category_list,
                    'menu_sort': menu_sort,
                    'name_sort': 'Категории:',
                    }       
    
    menu_sort[0]['active'] = False   
    menu_sort[1]['active'] = True
        
    return render(request, 'blog/archive.html', context=context)