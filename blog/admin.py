from django.contrib import admin
from .models import Article,Tag,Category
# Register your models here.
class ArticleAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
    list_display = ('title','created','status','category','preview_img')
"""     list_display = ('id','short_full_url_admin','short_symbol_url','short_phrase_url','pub_date_url','count_url')
    list_display_links = ('id','short_symbol_url','short_phrase_url','pub_date_url')
    search_fields = ('full_url','hort_phrase_url')
    list_editable = ('count_url',)
    list_filter = ('pub_date_url','count_url')

    def upper_case_name(self, obj):
        return ("%s %s" % (obj.first_name, obj.last_name)).upper()
    upper_case_name.short_description = 'Name' """

class TagAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}

class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("name",)}
    
admin.site.register(Article,ArticleAdmin)
admin.site.register(Tag,TagAdmin)
admin.site.register(Category,CategoryAdmin)
admin.site.site_header = 'Bolshakovsky.ru'
admin.site.site_title = 'Админка'
