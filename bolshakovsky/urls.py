"""Bolshakovsky URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from django.urls import path
from blog.views import index, archive, pageNotFound, show_article, show_tags, show_category
from dynaconf import settings as _settings

urlpatterns = [
    path(_settings.ADMIN_URL, admin.site.urls),
    path('', index, name='home'),
    path('archive/', archive, name='archive'),
    path('article/<slug:article_slug>/', show_article, name='article'),
    path('archive/tags/<slug:tags_slug>/', show_tags, name='tags'),
    path('archive/tags/', show_tags, name='tags_all'),
    path('archive/category/<slug:category_slug>/', show_category, name='category'),
    path('archive/category/', show_category, name='category_all'),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


handler404 = pageNotFound